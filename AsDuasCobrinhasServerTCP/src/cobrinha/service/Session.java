package cobrinha.service;

import cobrinha.model.Color;
import cobrinha.model.Game;
import cobrinha.model.Player;
import cobrinha.util.Util;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Session extends Thread {
    
    private final Integer maxPlayers = 1;
    private Game game;
    private final int rows = 20;
    private final int columns = 20;
    private final int maxScore = 20;
    private final Color[] colors = {Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW};
    private final ServerStatus serverStatus;
    private ServerSocket welcomeSocket = null;
    private ArrayList<Socket> clientSockets;
    
    public Session(ServerStatus serverStatus) {
        super();
        this.serverStatus = serverStatus;
    }
    
    @Override
    public void run() {
        
        System.out.println("SERVER STARTED");
        System.out.println("Max players: " + maxPlayers);
        serverStatus.setMaxPlayers(maxPlayers);
        
        try {
            welcomeSocket = new ServerSocket(Util.PORT_SERVER);
            
            connectPlayers(welcomeSocket);
        } catch (IOException | InterruptedException | ClassNotFoundException ex) {
            Logger.getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void connectPlayers(ServerSocket welcomeSocket) throws IOException, InterruptedException, ClassNotFoundException {
        int playerNumber = 1;
        ObjectInputStream inFromClient;
        ObjectOutputStream outToClient;
        List<ObjectOutputStream> outToClientList = new ArrayList<>();
        clientSockets = new ArrayList<>();
        game = new Game(rows, columns, maxScore);
        
        do {
            System.out.println("Waiting for Player " + playerNumber + " to connect..");
            Socket clientSocket = welcomeSocket.accept();
            
            inFromClient = new ObjectInputStream(clientSocket.getInputStream());
            outToClient = new ObjectOutputStream(clientSocket.getOutputStream());
            
            outToClientList.add(outToClient);
            clientSockets.add(clientSocket);

            System.out.println("Player " + playerNumber + " connected");
            outToClient.writeObject("Conexão estabelecida");
            outToClient.writeInt(playerNumber-1);
            outToClient.flush();

            String playerName = (String) inFromClient.readObject();
            if(playerName.isEmpty()) {
                playerName = colors[playerNumber-1].name();
            }

            Player player = new Player(playerName, colors[playerNumber-1]);
            this.game.addPlayer(player);
            
            ControlCommands controlCommands = new ControlCommands(inFromClient, player);
            Thread controlCommandsThread = new Thread(controlCommands, "Thread Control Commands");
            controlCommandsThread.start();
            
            serverStatus.setPlayersConnected(playerNumber);
            
            playerNumber++;
        } while (this.game.getPlayers().size() < maxPlayers);
        
        System.out.println("Starting game...");
        startGame(outToClientList);
    }
    
    private void startGame(List<ObjectOutputStream> outToClientList) throws InterruptedException, IOException, ClassNotFoundException {
        game.init();
        serverStatus.setGameStatus(ServerStatus.Status.GAME_STARTED);
        Starter gameStarter = new Starter(game, outToClientList);
        gameStarter.start();
        
        //se chegou aqui é pq deu algum problema, então volta a esperar conexões
        System.out.println("Restarting server...");
        
        serverStatus.resetStatus();
        closeConnections();
        
        //volta a esperar conexões
        connectPlayers(welcomeSocket);
    }

    private void closeConnections() {
        for(Socket client: clientSockets) {
            try {
                client.close();
            } catch (IOException ex) {
                Logger.getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
