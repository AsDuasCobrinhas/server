/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.service;

/**
 *
 * @author uira
 */
public class ServerStatus {

    private Status gameStatus;
    private int maxPlayers;
    private int playersConnected;

    public ServerStatus() {
        this.resetStatus();
    }
   
    public void resetStatus() {
        gameStatus = Status.WAITING_FOR_PLAYERS;
        playersConnected = 0;
    }

    public Status getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(Status gameStatus) {
        this.gameStatus = gameStatus;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public int getPlayersConnected() {
        return playersConnected;
    }

    public void setPlayersConnected(int playersConnected) {
        this.playersConnected = playersConnected;
    }
    
    public enum Status {
        GAME_STARTED, WAITING_FOR_PLAYERS
    }
    
}
