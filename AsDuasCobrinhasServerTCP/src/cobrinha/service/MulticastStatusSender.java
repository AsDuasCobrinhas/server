/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.service;

import cobrinha.util.Util;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uira
 */
public class MulticastStatusSender implements Runnable {
    
    ServerStatus serverStatus;

    public MulticastStatusSender(ServerStatus serverStatus) {
        this.serverStatus = serverStatus;
    }

    @Override
    public void run() {
        
        InetAddress multicastAddress;
        MulticastSocket multicastSocket;
        DatagramPacket packet;
        
        try {
            System.out.println("Multicast Status Sender: STARTED");
            
            multicastAddress = InetAddress.getByName(Util.MULTICAST_ADDRESS);
            multicastSocket = new MulticastSocket(Util.MULTICAST_PORT);
            multicastSocket.joinGroup(multicastAddress);
            
            byte[] msg = createStatusMsg();
            packet = new DatagramPacket(msg, msg.length);
            packet.setAddress(multicastAddress);
            packet.setPort(Util.MULTICAST_PORT);
            
            while(true) {
                msg = createStatusMsg();
                packet.setData(msg);
                packet.setLength(msg.length);
                
                multicastSocket.send(packet);
                Thread.sleep(1000);
            }
            
            //System.out.println("Sent a  multicast message.");
            //System.out.println("Exiting application");
            
        } catch (SocketException ex) {
            Logger.getLogger(MulticastStatusSender.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(MulticastStatusSender.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MulticastStatusSender.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastStatusSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private byte[] createStatusMsg() {
        StringBuilder msg = new StringBuilder();
        msg.append(serverStatus.getMaxPlayers());
        msg.append(',');
        msg.append(serverStatus.getPlayersConnected());
        msg.append(',');
        msg.append(serverStatus.getGameStatus().toString());
        return msg.toString().getBytes();
    }
    
}
