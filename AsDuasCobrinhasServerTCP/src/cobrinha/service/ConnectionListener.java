/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.service;

import cobrinha.model.Game;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author uira
 */
public interface ConnectionListener {
    public void onGameReceived(Game game, int playerID, ObjectInputStream inFromServer, ObjectOutputStream outToServer);
    public void onConnectionClose();
}
