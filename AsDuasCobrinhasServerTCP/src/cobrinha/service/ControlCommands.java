/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.service;

import cobrinha.model.Snake;
import cobrinha.model.Direction;
import cobrinha.model.Player;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ControlCommands implements Runnable {
    
    private Snake snake;
    private ObjectInputStream inFromClient;

    ControlCommands(ObjectInputStream inFromClient, Player player) {
        this.snake = player.getSnake();
        this.inFromClient = inFromClient;
    }

    @Override
    public void run() {
        Direction newDirection;
        
        try {
            while(true) {
                newDirection = (Direction) inFromClient.readObject();
                if(newDirection != Direction.getOpposite(snake.getDirection())) {
                    snake.setDirection(newDirection);
                }
            }

        } catch (IOException ex) {
            //System.err.println(ex.getCause());
        } catch (ClassNotFoundException ex) {
            //System.err.println(ex.getCause());
        }
    }
    
}
