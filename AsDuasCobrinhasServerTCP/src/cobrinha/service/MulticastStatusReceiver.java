/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.service;

import cobrinha.util.Util;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uira
 */
public class MulticastStatusReceiver extends Thread {
    
    ServerStatus serverStatus;
    StatusListener statusListener;
    MulticastSocket multicastSocketListener;

    public MulticastStatusReceiver() {
        super("Thread MulticastStatusReceiver");
    }

    @Override
    public void run() {
        System.out.println("Multicast Status Receiver: STARTED");
        
        InetAddress multicastAddress;
        
        DatagramPacket packet;
        try {
            
            multicastAddress = InetAddress.getByName(Util.MULTICAST_ADDRESS);
            multicastSocketListener = new MulticastSocket(Util.MULTICAST_PORT);
            multicastSocketListener.joinGroup(multicastAddress);
            multicastSocketListener.setSoTimeout(1000);
            
            while(true) {
                packet = new DatagramPacket(new byte[1024], 1024);

                try {
                    multicastSocketListener.receive(packet);
                    String msg = new String(packet.getData(), packet.getOffset(), packet.getLength());
                    //System.out.println(msg);
                
                    parseStatus(msg);
                    statusListener.setStatus(serverStatus);
                    
                    Thread.sleep(1000);
                } catch (SocketTimeoutException e) {
                    serverStatus = null;
                    System.out.println("SERVER OFFLINE");
                    statusListener.setStatus(serverStatus);
                }
                
            }
            
        } catch (UnknownHostException ex) {
            //Logger.getLogger(MulticastStatusReceiver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | InterruptedException ex) {
            //Logger.getLogger(MulticastStatusReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void setStatusListener(StatusListener statusListener) {
        this.statusListener = statusListener;
    }
    
    public ServerStatus getServerStatus() {
        return serverStatus;
    }

    private void parseStatus(String msg) {
        Scanner scan = new Scanner(msg).useDelimiter(",");
        int maxPlayers = scan.nextInt();
        int playersConnected = scan.nextInt();
        ServerStatus.Status gameStatus = ServerStatus.Status.valueOf(scan.next());
        
        serverStatus = new ServerStatus();
        serverStatus.setMaxPlayers(maxPlayers);
        serverStatus.setPlayersConnected(playersConnected);
        serverStatus.setGameStatus(gameStatus);
        
        scan.close();
    }
    
    public void disconnect() {
        if(multicastSocketListener != null) {
            multicastSocketListener.close();
        }
    }
    
}
