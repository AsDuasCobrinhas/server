/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.service;

import cobrinha.model.Game;
import cobrinha.Client;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;

/**
 *
 * @author danilo
 */
public class UpdateGame extends Thread {
    
    private ObjectInputStream inFromServer;
    private Client gui;

    public UpdateGame(Client gui, ObjectInputStream inFromServer) {
        super("Thread Update Game");
        this.gui = gui;
        this.inFromServer = inFromServer;
    }
    
    public UpdateGame(Client gui) {
        super("Thread Update Game");
        this.gui = gui;
    }
    
    @Override
    public void run() {
        try {
            this.updateGame();
        } catch (IOException ex) {
            System.out.println("IOException: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("Erro ClassNotFoundException: " + ex.getMessage());
        } finally {
            gui.resetClient(true);
        }
    }
    
    private void updateGame() throws IOException, ClassNotFoundException {
        Game game;

        while(true) {
            game = (Game) inFromServer.readObject();
            gui.update(game);
        }
    }
    
    public void disconnect() {
        try {
            inFromServer.close();
        } catch (IOException ex) {
            Logger.getLogger(UpdateGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
