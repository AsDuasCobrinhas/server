/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.service;

import cobrinha.model.Snake;
import cobrinha.model.Game;
import cobrinha.model.Player;
import cobrinha.model.SnakeNode;
import cobrinha.model.Symbol;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 *
 * @author danilo
 */
public class Starter{

    private Game game;
    private List<Player> players;
    private List<ObjectOutputStream> outToClientList;

    public Starter(Game game, List<ObjectOutputStream> outToClientList) {
        this.game = game;
        this.players = game.getPlayers();
        this.outToClientList = outToClientList;
    }
    
    public void start() {
        try {
             do {
                Thread.sleep(100);
                this.moveSnakes();
                game.printMatrix();
                
                checkScore();
                
                sendGameToPlayers();
                
                if(game.getWinner() != null) {
                    break;
                }
                
            } while (true);
        } catch (InterruptedException ex) {
            //System.err.println("Execução interrompida: " + ex.getMessage());
        } catch (IOException ex) {
            //System.err.println("Erro ao escrever objeto: " + ex.getMessage());
        }
    }

    private void moveSnakes() {
        this.players.forEach((player) -> {
            Snake snake = player.getSnake();
            snake.move();
            this.verifyCollision(snake);
            
            if(this.verifyEatingFood(snake)) {
                player.addPoint();
            }
            game.updateMatrix();
        });
    }
    
    private boolean verifyCollision(Snake snake) {
        SnakeNode snakeHead = snake.getHead();
        
        Symbol symbol = game.getMatriz()[snakeHead.getY()][snakeHead.getX()];
        if (symbol == Symbol.WALL || symbol == Symbol.SNAKE) {
            snake.cutHead();
            return true;
        }
        return false;
    }

    private boolean verifyEatingFood(Snake snake) {
        SnakeNode snakeHead = snake.getHead();

        Symbol symbol = game.getMatriz()[snakeHead.getY()][snakeHead.getX()];
        if (symbol == Symbol.FOOD) {
            snake.appendNode();
            game.randomFoodPosition();
            return true;
        }
        return false;
    }

    private void sendGameToPlayers() throws IOException {
        for (ObjectOutputStream outToClient: this.outToClientList) {
            outToClient.reset();
            outToClient.writeObject(game);

            outToClient.flush();
        }
    }

    private void checkScore() {
        for(Player player: game.getPlayers()) {
            if(player.getScore() == game.getMaxScore()) {
                game.setWinner(player);
            }
        }
    }

    
}
