/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.service;

import cobrinha.Client;
import cobrinha.model.Game;
import cobrinha.util.Util;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uira
 */
public class Connection extends Thread {
    
    private static ObjectOutputStream outToServer;
    private ConnectionListener connectionListener;
    private final String playerName;
    private Socket serverSocket;
    private int playerID = -1;

    public Connection(String playerName, Client listener) {
        this.playerName = playerName;
        this.connectionListener = listener;
    }
    
    @Override
    public void run() {
        try {
            //conectar com servidor
            serverSocket = new Socket(Util.SERVER_ADDRESS, Util.PORT_SERVER);
            outToServer = new ObjectOutputStream(serverSocket.getOutputStream());
            ObjectInputStream inFromServer = new ObjectInputStream(serverSocket.getInputStream());
            
            outToServer.writeObject(playerName);
            outToServer.flush();
            String reply = (String) inFromServer.readObject();
            playerID = (int) inFromServer.readInt();
            System.out.println(reply);
            
            Game game = (Game) inFromServer.readObject();
            
            
            if(connectionListener != null) {
                connectionListener.onGameReceived(game, playerID, inFromServer, outToServer);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            connectionListener.onConnectionClose();
        }
    }

    public void setOnGameReceivedListener(Client listener) {
        this.connectionListener = listener;
    }
    
    public int getPlayerID() {
        return playerID;
    }

    public void close() {
        try {
            outToServer.close();
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void disconnect() {
        try {
            serverSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
