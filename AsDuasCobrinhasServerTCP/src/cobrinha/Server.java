package cobrinha;

import cobrinha.service.MulticastStatusSender;
import cobrinha.service.ServerStatus;
import cobrinha.service.Session;
import java.io.IOException;

public class Server {

    public static void main(String[] args) throws IOException {

        ServerStatus serverStatus = new ServerStatus();

        Thread multicastStatusSender = new Thread(new MulticastStatusSender(serverStatus), "Thread MulticastStatusSender");
        multicastStatusSender.start();

        Session session = new Session(serverStatus);
        session.start();
    }
}
