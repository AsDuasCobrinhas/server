/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.view;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author uira
 */
public class HeaderView extends StackPane {

    private String title;
    private int width;
    
    public HeaderView(String title, int width) {
        super();
        this.title = title;
        this.width = width;
        configHeaderView();
    }
    
    private void configHeaderView() {
        Rectangle bg = new Rectangle(width, 60);
        bg.setFill(new Color(.1,.1,.1,1));
        Text titleView;
        titleView = new Text(title);
        titleView.setFill(Color.AQUAMARINE);
        titleView.setFont(Font.font("monospace",34));
        
        this.getChildren().add(bg);
        this.getChildren().add(titleView);
    }
    
}
