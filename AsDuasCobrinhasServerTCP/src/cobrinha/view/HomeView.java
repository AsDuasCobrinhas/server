package cobrinha.view;

import cobrinha.service.ServerStatus;
import cobrinha.service.StatusListener;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class HomeView extends AnchorPane implements StatusListener {
    
    private HeaderView header;
    private int width;
    private int marginTop;
    private Button playButton;
    private TextField nameInput;

    
    private Text serverStatusText;
    
    public HomeView(String gameTitle, int width) {
        super();
        
        this.header = new HeaderView(gameTitle, width);
        this.width = width;
        this.marginTop = 100;
        
        configView();
    }

    private void configView() {
        
        setStyle("-fx-background-color: #191919");

        StackPane bg = new StackPane();
        bg.setLayoutY(marginTop);
        bg.setMinHeight(width);
        bg.setMinWidth(width);
        
        Text label = new Text("Digite seu nome:");
        label.setFont(Font.font("monospace", FontWeight.BOLD, 20));
        label.setFill(Color.WHITE);
        label.setLayoutX(90);
        label.setLayoutY(180);
        label.setWrappingWidth(200);
        label.setTextAlignment(TextAlignment.CENTER);
        
        nameInput= new TextField();
        nameInput.setPrefHeight(40);
        nameInput.setPrefWidth(200);
        nameInput.setLayoutX(90);
        nameInput.setLayoutY(200);
        nameInput.setAlignment(Pos.CENTER);
        nameInput.fontProperty().set(Font.font("monospace", FontWeight.BOLD, 18));
        
        playButton = new Button("JOGAR", Color.DARKCYAN);
        playButton.setLayoutPosition(90, 260);
        playButton.requestFocus();

        this.getChildren().add(header);
        this.getChildren().add(bg);
        this.getChildren().add(label);
        this.getChildren().add(nameInput);
        this.getChildren().add(playButton);
        
        configStatusView();
    }

    public Button getPlayButton() {
        return playButton;
    }
    
    public TextField getNameInput() {
        return nameInput;
    }
    
    public String getName() {
        return nameInput.getText().trim();
    }

    private void configStatusView() {
        Rectangle bg = new Rectangle(width, 20);
        bg.setFill(new Color(.05,.05,.05,1));
        
        serverStatusText = new Text();
        serverStatusText.setFont(Font.font("monospace", FontWeight.NORMAL, 12));
        //maxScore.setWrappingWidth(width);
        //maxScore.setTextAlignment(TextAlignment.CENTER);
        serverStatusText.setFill(Color.AQUAMARINE);
        
        StackPane statusView = new StackPane(bg, serverStatusText);
        statusView.setLayoutY(460);
        
        this.getChildren().add(statusView);
    }

    @Override
    public void setStatus(ServerStatus serverStatus) {
        StringBuilder str = new StringBuilder();
        if(serverStatus == null) {
            str.append("SERVIDOR OFFLINE");
            serverStatusText.setFill(Color.RED);
        } else {            
            if(serverStatus.getPlayersConnected() == 0) {
                playButton.enable();
            }
            switch (serverStatus.getGameStatus()) {
                case GAME_STARTED:
                    str.append("Jogo iniciado. Aguarde a próxima partida");
                    serverStatusText.setFill(Color.BLUE);
                    playButton.disable("Aguarde...");
                    break;
                case WAITING_FOR_PLAYERS:
                    str.append("Servidor ONLINE. ");
                    str.append("Aguardando jogadores (");
                    str.append(serverStatus.getPlayersConnected());
                    str.append('/');
                    str.append(serverStatus.getMaxPlayers());
                    str.append(')');
                    serverStatusText.setFill(Color.GREEN);
                    break;
            }
            
        }
        
        serverStatusText.setText(str.toString());
    }
}
