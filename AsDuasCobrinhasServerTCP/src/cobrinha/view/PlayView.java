package cobrinha.view;

import cobrinha.model.Player;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class PlayView extends AnchorPane {
    
    private GameView gameView;
    private Node header;
    private int width;
    private HBox scoreView;
    private final ArrayList<Text> listScores = new ArrayList<>();
    private int playerID = -1;
    
    public PlayView(int playerID, GameView gameView, String gameTitle, int width) {
        this.playerID = playerID;
        this.width = width;
        this.gameView = gameView;
        this.header = new HeaderView(gameTitle, width);
        
        paddingProperty().set(new Insets(0));
        setStyle("-fx-background-color: #191919");
        
        addChild(this.header);
        this.header.setLayoutY(0);
        
        configScoreView();
        
        gameView.setLayoutY(90);

        addChild(gameView);
        
        configureMaxScoreInfo();
    }
    
    private void addChild(Node node) {
        getChildren().add(node);
    }

    private void configScoreView() {
        scoreView = new HBox();
        
        Rectangle bg = new Rectangle();
        bg.setFill(new javafx.scene.paint.Color(.1,.1,.1,1));
        bg.setWidth(gameView.getGameWidth() - gameView.getMOD());
        bg.setHeight(30);
        
        scoreView.setAlignment(Pos.CENTER);
        
        Text playerScore;
        Text playerName;
        HBox playerInfo;
        List<Player> players = gameView.getGame().getPlayers();
        for(Player player: players) {
            
            if(player.getName().equals(player.getColor().toString())) {
                if(players.indexOf(player) == playerID) {
                    playerName = new Text(player.getName() + "(você): ");
                } else {
                    playerName = new Text(player.getColor().toString() + ": ");
                }
                
            } else {
                playerName = new Text(player.getName() + ": ");
            }
            
            playerScore = new Text(String.valueOf(player.getScore()));
            
            playerScore.setFill(player.getColor().getColorFX());
            playerName.setFill(player.getColor().getColorFX());
            
            if(players.indexOf(player) == playerID) {
                playerScore.setFont(Font.font("monospace", FontWeight.BOLD, 14));
                playerName.setFont(Font.font("monospace", FontWeight.BOLD, 14));
            } else {
                playerScore.setFont(Font.font("monospace", FontWeight.NORMAL, 14));
                playerName.setFont(Font.font("monospace", FontWeight.NORMAL, 14));
            }
            
            listScores.add(playerScore);
            
            playerInfo = new HBox(playerName, playerScore);
            scoreView.setMargin(playerInfo, new Insets(0, 10, 0, 10));
            
            scoreView.getChildren().add(playerInfo);
        }
        
        StackPane view = new StackPane(bg, scoreView);
        view.setLayoutY(65);
        addChild(view);
    }

    public void updateScore() {
        List<Player> players = gameView.getGame().getPlayers();
        for(int i = 0; i < players.size(); i++) {
            listScores.get(i).setText(String.valueOf(players.get(i).getScore()));
        }
    }

    private void configureMaxScoreInfo() {
        
        Rectangle bg = new Rectangle(width, 20);
        bg.setFill(new Color(.05,.05,.05,1));
        
        Text maxScore = new Text("QUEM FIZER " + gameView.getGame().getMaxScore() + " PONTOS GANHA!!!");
        maxScore.setFont(Font.font("monospace", FontWeight.NORMAL, 12));
        //maxScore.setWrappingWidth(width);
        //maxScore.setTextAlignment(TextAlignment.CENTER);
        maxScore.setFill(Color.AQUAMARINE);
        
        StackPane maxScoreInfo = new StackPane(bg, maxScore);
        maxScoreInfo.setLayoutY(460);
        
        addChild(maxScoreInfo);
    }

    public GameView getGameView() {
        return gameView;
    }
}
