package cobrinha.view;

import cobrinha.model.Direction;
import cobrinha.model.Game;
import cobrinha.model.Player;
import cobrinha.model.Snake;
import cobrinha.model.SnakeNode;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;

public class GameView extends AnchorPane {
    private Game game;
    private final int MOD;
    private final int width;
    private final int height;
    private final int rows;
    private final int columns;
    
    private Polygon foodView;
    private final ArrayList<AnchorPane> snakesView;

    public GameView(Game game, int mod) {
        super();
        this.game = game;
        this.MOD = mod;
        width = game.getColumns() * mod - mod;
        height = game.getRows() * mod - mod;
        rows = game.getRows();
        columns= game.getColumns();
        snakesView = new ArrayList<>();
        configView();
        draw();
    }

    public int getGameWidth() {
        return width;
    }

    public int getGameHeight() {
        return height;
    }

    public int getMOD() {
        return MOD;
    }

    public Game getGame() {
        return game;
    }
    
    private void configView() {
        Rectangle clippingMask = new Rectangle(0, 0, width, height);
        this.setPadding(new Insets(0,0,0,0));
        this.setClip(clippingMask);
    }
    
    private void draw() {
        drawMap();
        drawGrid();
        drawFood();
        drawSnakes();
        drawSnakesHome();
    }
    
    private void drawFood() {
        int x = game.getFood().getX();
        int y = game.getFood().getY();
        foodView = new Polygon(
            MOD/2, MOD/4,
            MOD -MOD/4, MOD/2,
            MOD/2, MOD -MOD/4,
            MOD/4,MOD/2
        );
        foodView.setLayoutX(x*MOD - MOD/2);
        foodView.setLayoutY(y*MOD - MOD/2);
        
        foodView.setFill(Color.DEEPPINK);

        this.getChildren().add(foodView);
    }
    
    private void setNewFoodPosition() {
        foodView.setLayoutX(game.getFood().getX()*MOD - MOD/2);
        foodView.setLayoutY(game.getFood().getY()*MOD - MOD/2);
    }
    
     private void drawSnakes() { 
        Polyline path;
        Shape headView;
        AnchorPane snakeView;
        Snake snake;
        double strokeWidth = MOD - ((int) (MOD/2));
        double[] points;
         
        for(Player player : game.getPlayers()) {
            snake = player.getSnake();
            points = snake.getPoints();
            applyModToPoints(points);
            
            //configuração do corpo
            path = new Polyline(points);
            path.setStroke(player.getColor().getColorFX());
            path.setStrokeWidth(strokeWidth);
            path.setStrokeLineCap(StrokeLineCap.ROUND);
            path.setStrokeLineJoin(StrokeLineJoin.ROUND);
            
            //configuração da cabeça
            headView = new Circle(strokeWidth/2, player.getColor().getColorFX());
            setSnakeHeadPosition(headView, snake.getHead(), snake.getDirection());

            snakeView = new AnchorPane(path, headView);
            
            snakesView.add(snakeView);
            this.getChildren().add(snakeView);
        }
     }

     
     private void setNewSnakesPlaces() {
        Polyline path;
        Node headView;
        Snake snake;
        for(int i = 0; i < snakesView.size(); i++) {
            snake = game.getPlayers().get(i).getSnake();
            double[] points = snake.getPoints();
            applyModToPoints(points);
            
            path = (Polyline) snakesView.get(i).getChildren().get(0);
            path.getPoints().clear();
            for(int k = 0; k < points.length; k++) {
                path.getPoints().add(points[k]);
            }
            headView = snakesView.get(i).getChildren().get(1);
            setSnakeHeadPosition(headView, snake.getHead(), snake.getDirection());

        }
    }
     
    private void setSnakeHeadPosition(Node headView, SnakeNode head, Direction direction) {
            int headDisplaceX = 0;
            int headDisplaceY = 0;
            int width = (int) headView.getLayoutBounds().getWidth();
            int height = (int) headView.getLayoutBounds().getHeight();
            switch (direction) {
                case UP: headDisplaceY = -MOD/5; break;
                case DOWN: headDisplaceY = MOD/5; break;
                case LEFT: headDisplaceX = -MOD/5; break;
                case RIGHT: headDisplaceX = MOD/5; break;
            }
            headView.setLayoutX(head.getX()*MOD + headDisplaceX);
            headView.setLayoutY(head.getY()*MOD + headDisplaceY);
    }
     
     private void applyModToPoints(double[] points) {
        for(int i = 0; i < points.length; i++) {
          points[i] = points[i]*MOD;
        }
     }
     
     private void drawMap() {
        Color bgColor = new Color(.12,.12,.12,1);
        Color borderColor = new Color(.05,.05,.05,1);
        
        Rectangle background = new Rectangle(0, 0, width, height);
        background.setFill(bgColor);
        background.setStroke(borderColor);
        background.setStrokeWidth(MOD/2);
        background.getStrokeDashArray().add(2d);
        background.setStrokeType(StrokeType.INSIDE);
        
        this.getChildren().add(background);
     }
     
     private void drawGrid() {
        Shape line;
        int m = MOD/2;
        Color gridColor = new Color(1d,1d,1d,1d);
        for(int y = 1, x = 1; y < rows-2 || x < columns-2; y++, x++) {
            line = new Line(m, y*MOD + m, columns*MOD-MOD-m, y*MOD+m);
            line.setStrokeWidth(.1);
            line.setStroke(gridColor);
            this.getChildren().add(line);
            
            line = new Line(x*MOD+m, m, x*MOD+m, rows*MOD-MOD-m);
            line.setStrokeWidth(.1);
            line.setStroke(gridColor);
            this.getChildren().add(line);
        }
     }

    private void drawSnakesHome() {
        Snake snake;
        for(Player player : game.getPlayers()) {
            snake = player.getSnake();
            int x = snake.getStartX();
            int y = snake.getStartY();
            //Rectangle snakeHome = new Rectangle(snake.getStartX()*MOD, snake.getStartY()*MOD-MOD/2, (MOD/2) + MOD/10, MOD);
            Polygon snakeHome = new Polygon(
                    x*MOD,
                    y*MOD - MOD/2 - MOD/4,
                    
                    x*MOD + MOD/2,
                    y*MOD - MOD/2,
                    
                    x*MOD + MOD*3/8,
                    y*MOD - MOD/4,
                    
                    x*MOD + MOD/8  + MOD/5,
                    y*MOD,
                    
                    x*MOD + MOD*3/8,
                    y*MOD + MOD/4,
                    
                    x*MOD + MOD/2,
                    y*MOD + MOD/2,
                    
                    x*MOD,
                    y*MOD + MOD/2 + MOD/4

                    );
            
            snakeHome.setFill(player.getColor().getColorFX().darker().darker().darker());
            this.getChildren().add(snakeHome);
        }
    }

    public void update(Game game) {
        this.game = game;
        updateViews();
    }
    
    private void updateViews() {
        setNewFoodPosition();
        setNewSnakesPlaces();
    }

}
