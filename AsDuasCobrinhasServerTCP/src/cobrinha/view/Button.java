package cobrinha.view;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;


public class Button extends StackPane {
        
    private String title;
    private Color bgColor;
    private Color titleColor;
    private Rectangle bg;
    private Text titleView;
    private final Color disabledColor = Color.GRAY;

    public Button(String title, Color color) {
        super();
        this.title = title;
        this.bgColor = color;
        this.titleColor = Color.WHITE;

        configureBackground();
        configureTitle();

        configureClickEffect();
        setCursor(Cursor.HAND);
        setEffect(new DropShadow(10, Color.BLACK));
    }

    private void configureBackground() {
        bg = new Rectangle(200, 60, bgColor);
        
        bg.setArcHeight(10);
        bg.setArcWidth(10);

        this.getChildren().add(bg);
    }

    private void configureTitle() {
        titleView = new Text(title);
        titleView.setFill(Color.WHITE);
        titleView.setFont(Font.font("monospace", FontWeight.BOLD,18));
        titleView.setTextAlignment(TextAlignment.CENTER);
        this.getChildren().add(titleView);
    }
    
    public void setTitleColor(Color color) {
        this.titleColor = color;
        titleView.setFill(titleColor);
    }

    public void setWidth(int newWidth) {
        super.setWidth(newWidth);
        bg.setWidth(newWidth);
        
    }

    public void setHeight(int newHeight) {
        super.setHeight(newHeight);
        bg.setHeight(newHeight);
    }

    public void setLayoutPosition(int x, int y) {
        this.setLayoutX(x);
        this.setLayoutY(y);
    }

    private void configureClickEffect() {
        this.setOnMousePressed((MouseEvent event) -> {
            bg.setFill(bgColor.brighter());
            titleView.setFill(bgColor.darker());
        });
        this.setOnMouseReleased((MouseEvent event) -> {
            bg.setFill(bgColor);
            titleView.setFill(titleColor);
        });
    }

    public void disable(String tempTitle) {
        this.setDisable(true);
        this.bg.setFill(disabledColor);
        this.titleView.setFill(disabledColor.darker());
        this.titleView.setText(tempTitle);
    }
    
    public void enable() {
        this.setDisable(false);
        this.bg.setFill(bgColor);
        this.titleView.setFill(titleColor);
        this.titleView.setText(title);
    }

    public void setTitle(String newTitle) {
        this.title = newTitle;
        titleView.setText(title);
    }
}