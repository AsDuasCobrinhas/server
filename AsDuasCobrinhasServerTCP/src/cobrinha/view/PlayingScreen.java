/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.view;

import cobrinha.model.Player;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author uira
 */
public class PlayingScreen extends Scene {
    PlayView playView;
    GameView gameView;

    public PlayingScreen(PlayView playView) {
        super(playView);
        this.playView = playView;
        gameView = playView.getGameView();
    }


    public Button showWinner() {
        Player winner = gameView.getGame().getWinner();
        StackPane winnerDialog;
        
        Rectangle bg = new Rectangle(240, 180);
        bg.setFill(new Color(.1,.1,.1,1));
        bg.setEffect(new DropShadow(30, Color.BLACK));
        bg.setArcHeight(30);
        bg.setArcWidth(30);
        bg.setStroke(Color.DARKGRAY.darker().darker().darker());
        bg.setStrokeWidth(2);
        
        VBox content;
        Text title = new Text("Fim de jogo!");
        title.setFont(Font.font("monospace", FontWeight.BOLD, 24));
        title.setFill(Color.WHITE);
        
        Text winnerMessage = new Text(winner.getName() + " é o vencedor.");
        winnerMessage.setFont(Font.font("monospace", FontWeight.BOLD, 14));
        winnerMessage.setFill(winner.getColor().getColorFX());
        
        Button homeButton = new Button("INÍCIO", Color.DARKCYAN);
        homeButton.setWidth(120);
        homeButton.setHeight(40);
        
        content = new VBox(20, title, winnerMessage, homeButton);
        content.setAlignment(Pos.CENTER);
        
        winnerDialog = new StackPane(bg, content);
        winnerDialog.setLayoutX(70);
        winnerDialog.setLayoutY(180);
        playView.getChildren().add(winnerDialog);
        
        return homeButton;
    }
}
