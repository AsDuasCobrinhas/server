package cobrinha;

import cobrinha.model.Direction;
import cobrinha.model.Game;
import cobrinha.service.Connection;
import cobrinha.service.MulticastStatusReceiver;
import cobrinha.service.UpdateGame;
import cobrinha.view.Button;
import cobrinha.view.GameView;
import cobrinha.view.HomeView;
import cobrinha.view.PlayView;
import cobrinha.view.PlayingScreen;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import cobrinha.service.ConnectionListener;
import cobrinha.service.ServerStatus;

public class Client extends Application implements ConnectionListener {
    
    private int MOD;
    private int playerID = -1;
    private String gameTitle;
    private GameView gameView;
    private PlayView playView;
    private HomeView homeView;
    private Stage window;
    private PlayingScreen playingScreen;
    private Scene homeScreen;
    private Game game;
    private static UpdateGame updateGameThread;
    private static MulticastStatusReceiver multicastStatusReceiver;
    private Connection connection;
    
    private static ObjectOutputStream outToServer;
    private int esc = 0;
    
    public static void main(String[] args) {        
        launch(args);
    }
    

    @Override
    public void start(Stage mainWindow) throws Exception {
        
        window = mainWindow;
        MOD = 20;
        gameTitle = "AS DUAS COBRINHAS";
        window.setTitle(gameTitle);
        
        homeView = new HomeView(gameTitle, MOD*MOD-MOD);
        homeScreen = new Scene(homeView);

        configureHomePlayButton();
        
        multicastStatusReceiver = new MulticastStatusReceiver();
        multicastStatusReceiver.start();
        multicastStatusReceiver.setStatusListener(homeView);

        window.setScene(homeScreen);
        window.setResizable(false);
        window.show();
    }
    
    @Override
    public void stop() {
        //fecha as conexões e interrompe todas as threads
        if(connection != null) {
            connection.disconnect();
            connection.interrupt();
        }
        if(updateGameThread != null) {
            updateGameThread.disconnect();
            updateGameThread.interrupt();
        }
        if(multicastStatusReceiver != null) {
            multicastStatusReceiver.disconnect();
            multicastStatusReceiver.interrupt();
        }
    }

    public void setModule(int module) {
        MOD = module;
    }

    public void update(Game game) {
        gameView.update(game);
        playView.updateScore();
        
        checkWinner();
    }
    
    private void configurePlayingEvents() {
        //Teclas de controle durante jogo
        playingScreen.setOnKeyPressed((KeyEvent event) -> {
            Direction direction;
            switch (event.getCode()) {
                case UP:
                    System.out.println("up");
                    direction = Direction.UP;
                    break;
                case DOWN:
                    System.out.println("down");
                    direction = Direction.DOWN;
                    break;
                case LEFT:
                    System.out.println("left");
                    direction = Direction.LEFT;
                    break;
                case RIGHT:
                    System.out.println("right");
                    direction = Direction.RIGHT;
                    break;
                case ESCAPE:
                    //encerrar jogo e voltar pra tela inicial
                    esc++;
                    if(esc == 3) {
                        resetClient(true);
                    }
                    return;
                default: return;
            }
            
            try {
                outToServer.writeObject(direction);
            } catch (IOException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
    }

    private void configureHomePlayButton() {
        homeView.getPlayButton().setOnMouseClicked ((event) -> {
            ServerStatus serverstatus = multicastStatusReceiver.getServerStatus();
            if(serverstatus != null) {
                //conectar como jogador
                if(serverstatus.getGameStatus() == ServerStatus.Status.WAITING_FOR_PLAYERS)
                if (connection == null) {
                    connection = new Connection(homeView.getName(), this);
                    connection.start();
                    
                    homeView.getNameInput().disableProperty().setValue(Boolean.TRUE);
                    homeView.getPlayButton().disable("Aguarde...");
                }
            }
        });
    }

    private void configurePlayingScene() {
        gameView = new GameView(game, MOD);
        playView = new PlayView(playerID, gameView, gameTitle, game.getColumns()*MOD-MOD);
        playingScreen = new PlayingScreen(playView);
        configurePlayingEvents();
    }

    private void checkWinner() {
        if(gameView.getGame().getWinner() != null) {
            Platform.runLater(() -> {
                Button homeButton = playingScreen.showWinner();
                homeButton.setOnMouseClicked((event) -> {
                    window.setScene(homeScreen);
                    resetClient(false);                   
                });
            });
        }
    }
    
    public void resetClient(Boolean gotoHomeScreen) {
        playerID = -1;
        esc = 0;
        gameView = null;
        playView = null;
        playingScreen = null;
        homeView.getNameInput().disableProperty().setValue(Boolean.FALSE);
        homeView.getPlayButton().enable();
        if(connection != null) {
            connection.close();
            connection.interrupt();
            connection = null;
        }
        if(updateGameThread != null) {
            updateGameThread.disconnect();
            updateGameThread.interrupt();
        }
        if(gotoHomeScreen) {
            window.setScene(homeScreen);
        }
    }
    
    @Override
    public void onGameReceived(Game game, int playerID, ObjectInputStream inFromServer, ObjectOutputStream outToServer) {
        this.playerID = playerID;
        this.game = game;
        this.outToServer = outToServer;
        configurePlayingScene();
        Platform.runLater(() -> {
            window.setScene(playingScreen);
        });

        updateGameThread = new UpdateGame(this, inFromServer);
        updateGameThread.start();
    }

    @Override
    public void onConnectionClose() {
        resetClient(true);
    }

    public Connection getConnection() {
        return connection;
    }
    
}
