package cobrinha.model;

import java.io.Serializable;

public class Player implements Serializable {
    private Integer id;
    private String name;
    private int score;
    private Snake snake;
    private Color color;

    public Player(String name, Color color) {
        this.name = name;
        this.color = color;
        this.snake = new Snake(this);
        
        this.score = 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Snake getSnake() {
        return snake;
    }

    public void setSnake(Snake snake) {
        this.snake = snake;
    }

    public void addPoint() {
        score++;
    }
    
}
