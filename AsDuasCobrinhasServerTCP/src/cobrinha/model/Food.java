package cobrinha.model;

import java.io.Serializable;

public class Food implements Serializable {
    private int x;
    private int y;

    public Food() {
        
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
