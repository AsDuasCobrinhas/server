/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobrinha.model;

import java.io.Serializable;

/**
 *
 * @author danilo
 */
public enum Color implements Serializable {
    BLUE(javafx.scene.paint.Color.DODGERBLUE),
    YELLOW(javafx.scene.paint.Color.GOLDENROD),
    RED(javafx.scene.paint.Color.CRIMSON),
    GREEN(javafx.scene.paint.Color.CHARTREUSE);
    
    private final javafx.scene.paint.Color color;

    private Color(javafx.scene.paint.Color color) {
        this.color = color;
    }

    public javafx.scene.paint.Color getColorFX() {
        return color;
    }
}
