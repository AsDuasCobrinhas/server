package cobrinha.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Snake implements Serializable {
    private SnakeNode head;
    private SnakeNode tail;
    private Direction direction;
    private int size;
    private final List<SnakeNode> nodes;
    private int startX;
    private int startY;
    private int startSize;
    private Direction startDirection;
    private Player player;

    public Snake(Player player) {
        this.player = player;
        nodes = new ArrayList<>();
    }
    
    public List<SnakeNode> getNodes() {
        return nodes;
    }

    public SnakeNode getHead() {
        return head;
    }
    
    public SnakeNode getTail() {
        return tail;
    }

    public Direction getDirection() {
        return direction;
    }
    
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void move() {
        shiftNodes();
        switch (direction) {
            case UP:
                head.moveUp(); break;
            case DOWN:
                head.moveDown(); break;
            case LEFT:
                if(head.getX() != 0)
                head.moveLeft(); break;
            case RIGHT:
                head.moveRight(); break;
        }
    }

    private void shiftNodes() {
        SnakeNode current = tail;
        
        int i = nodes.indexOf(tail);
        
        while(i > 0) {
            SnakeNode prev = nodes.get(i-1);
            
            current.copy(prev);
            
            current = nodes.get(i-1);
            i--;
        }
    }

    public void createSnake(int x, int y, int size) {
        SnakeNode startNode = new SnakeNode(x, y);
        head = startNode;
        tail = startNode;
        nodes.add(startNode);
        this.size = 1;
        
        for(int i = 0; i < size-1; i++) {
            appendNode();
        }
    }
    
    public void appendNode() {
        SnakeNode node = new SnakeNode(tail.getX(), tail.getY());
        nodes.add(node);
        tail = node;
        size++;
    }
    
    public void cutHead() {
        if(nodes.size() > 2) {
            nodes.remove(head);
            head = nodes.get(0);
            size--;
        } else {
            resetSnake();
        }
    }
    
    public void dropTail() {
        if(nodes.size() > 2) {
            nodes.remove(tail);
            tail = nodes.get(nodes.size()-1);
            size--;
        } else {
            resetSnake();
        }
    }

    void setStartPosition(int x, int y) {
        startX = x;
        startY = y;
    }

    public int getStartX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public int getStartSize() {
        return startSize;
    }

    public void setStartSize(int startSize) {
        this.startSize = startSize;
    }

    public int getSize() {
        return size;
    }

    public Direction getStartDirection() {
        return startDirection;
    }

    public void setStartDirection(Direction startDirection) {
        this.startDirection = startDirection;
    }

    void resetSnake() {
        int playerScore = this.getPlayer().getScore();
        getPlayer().setScore(playerScore - playerScore/3);
        
        nodes.clear();
        direction = startDirection;
        createSnake(startX, startY, startSize);
    }

    public double[] getPoints() {
        double[] points = new double[size*2];
        SnakeNode node;
        for(int i = 0; i < size; i++) {
            node = nodes.get(i);
            points[i*2] = node.getX();
            points[i*2 + 1] = node.getY();
        }
        return points;
    }

    private Player getPlayer() {
        return this.player;
    }
}