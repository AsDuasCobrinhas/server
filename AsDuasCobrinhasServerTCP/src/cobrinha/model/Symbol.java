package cobrinha.model;

import java.io.Serializable;

public enum Symbol implements Serializable {
    WALL,
    BLANK,
    SNAKE,
    FOOD;
}
