package cobrinha.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game implements Serializable {
    private int rows;
    private int columns;
    private Symbol[][] matrix;
    private int maxScore;
    private Food food;
    private List<Player> players;
    private Player winner;

    public Game(int rows, int columns, int maxScore) {
        this.maxScore = maxScore;
        this.rows = rows;
        this.columns = columns;
        matrix = new Symbol[rows][columns];
        clearMatrix();
        
        players = new ArrayList<>();
        food = new Food();
    }

    public void init() {
        randomFoodPosition();
        createSnakes();
        updateMatrix();
    }
    
    public int getMaxScore() {
        return this.maxScore;
    }

    public void setWinner(Player player) {
        this.winner = player;
    }
    
    public Player getWinner() {
        return this.winner;
    }
    
    public List<Player> getPlayers() {
        return players;
    }
    
    public void addPlayer(Player player) {
        players.add(player);
    }
    
    public void updateMatrix() {
        clearMatrix();
        placeSnakes();
        placeFood();
    }

    public Symbol[][] getMatriz() {
        return matrix;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public Food getFood() {
        return food;
    }
    
    private void clearMatrix(){ //preenche as bordas do mapa com -1
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                matrix[i][j] = Symbol.BLANK; //zera a matriz inteira
            }
        }
        //forma as bordas da matriz
        for(int i = 0; i < rows; i++) {
            matrix[i][0] = Symbol.WALL;    //primeira coluna inteira
            matrix[i][columns-1] = Symbol.WALL; //ultima coluna inteira
        }
        for(int i = 0; i < columns; i++) {
            matrix[0][i] = Symbol.WALL;    //primeira linha inteira
            matrix[rows-1][i] = Symbol.WALL; //ultima linha inteira
        }          
    }
    
    private void placeSnakes() {
        for(Player p : players) {
            for (SnakeNode n : p.getSnake().getNodes()) {
                matrix[n.getY()][n.getX()] = Symbol.SNAKE;
            }
        }
    }
    
    private void placeFood() {
        matrix[food.getY()][food.getX()] = Symbol.FOOD;
    }
    
    public void printMatrix() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                switch (matrix[i][j]) {
                    case WALL: System.out.print('█'); break;
                    case BLANK: System.out.print(' '); break;
                    case SNAKE: System.out.print('0'); break;
                    case FOOD: System.out.print('¤'); break;
                }
            }
            System.out.print('\n');
        }
    }
    
    private void createSnakes() {
        Snake snake;
        Direction startDirection = Direction.RIGHT;
        int startSize = 5;
        for(int i = 0; i < players.size(); i++) {
            snake = players.get(i).getSnake();
            snake.setStartPosition(0, (rows/(players.size()+1))*(i+1));
            snake.setStartSize(startSize);
            snake.setStartDirection(startDirection);
            snake.setDirection(startDirection);
            snake.resetSnake();
        }
        placeSnakes();
    }
    
    public void randomFoodPosition() {
        int x;
        int y;
        int maxX = columns-1;
        int maxY = rows-1;
        int min = 1;
        Random rand = new Random();
        do {
            x = rand.nextInt(maxX - min) + 1;
            y = rand.nextInt(maxY - min) + 1;
        } while (matrix[y][x] != Symbol.BLANK);
        food.setPosition(x, y);
    }

}
