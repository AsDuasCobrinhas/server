package cobrinha.model;

import java.io.Serializable;

public enum Direction implements Serializable {
    UP, DOWN, LEFT, RIGHT;
    
    public static Direction getOpposite(Direction direction) {
        switch (direction) {
            case UP: return DOWN;
            case DOWN: return UP;
            case LEFT: return RIGHT;
            case RIGHT: return LEFT;
            default: return direction;
        }
    }
}
